package Strings;

import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class WordFrequency {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Desired String");
		String str = sc.nextLine();
		wordfrequency(str);

	}

	private static void wordfrequency(String str) {
		// TODO Auto-generated method stub
		
		Map<String,Integer> mp = new TreeMap<>();
		String arr[] = str.split(" ");
		
		for(int i=0;i<arr.length;i++){
			if(mp.containsKey(arr[i])){
				mp.put(arr[i], mp.get(arr[i])+1);
			}else{
				mp.put(arr[i], 1);
			}
		}
		for(Map.Entry<String, Integer> entry:mp.entrySet()){
			System.out.println(entry.getKey()+" "+entry.getValue());
		}
	}

}
