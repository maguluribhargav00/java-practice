package Strings;

import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class WordFrequency1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Desired String");
		String str = sc.nextLine();
		wordfrequency(str);
		sc.close();

	}

	private static void wordfrequency(String str) {
		// TODO Auto-generated method stub
		Map<String,Integer> word = new TreeMap<>();
		String[] arr = str.split(" ");
		for(int i=0;i<arr.length;i++){
			if(word.containsKey(arr[i])){
				word.put(arr[i], word.get(arr[i])+1);
			}else{
				word.put(arr[i], 1);
			}
		}
		for(Map.Entry<String, Integer> entry:word.entrySet()){
			System.out.println(entry.getKey()+" "+entry.getValue());
		}
 		
	}

}
