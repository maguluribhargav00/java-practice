package Strings;

import java.util.Scanner;

public class SearchingCharacterinString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the desired String");
		String str = sc.next();
		char ch[] = str.toCharArray();
		char searchFor = 'g';
		sc.close();
		for(int i=0;i<ch.length;i++){
			if(ch[i] == searchFor){
				System.out.println(i);
				break;
			}else{
				System.out.println("The searching character is not found in the given string");
			}
		}
		

	}

}
