package Strings;

import java.util.HashSet;
import java.util.Scanner;

public class MissingCharacters1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the desired String");
		String str = sc.nextLine();
		MissingCharacter(str);

	}

	private static void MissingCharacter(String str) {
		// TODO Auto-generated method stub
		HashSet<Character> presentchars = new HashSet<>();
		for(int i=0;i<str.length();i++){
			char ch = str.charAt(i);
			if(ch>='a' && ch<='z'){
				presentchars.add(ch);
			}else if(ch>='A' && ch<= 'Z'){
				presentchars.add(Character.toLowerCase(ch));
			}
		}
		
		StringBuilder missingchars = new StringBuilder();
		for(char ch='a';ch<='z';ch++){
			if(!presentchars.contains(ch)){
				missingchars.append(ch);
			}
		}
		
		if(missingchars.length()==0){
			System.out.println("Stirng is pangram");
		}else{
			System.out.println(missingchars);
		}
		
	}

}
