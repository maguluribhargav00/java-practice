package Threads;


public class ThreadImplementation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Thread1 obj1 = new Thread1();
		Thread2 obj2 = new Thread2();
		
		//Getting the priority for the threads
/*		System.out.println(obj1.getPriority());
		System.out.println(obj2.getPriority());*/
		
		//Setting the priority for the thread using thread class
		/*obj2.setPriority(Thread.MAX_PRIORITY);
		System.out.println(obj2.getPriority());*/
		
		obj1.start();
		obj2.start();

	}

}
