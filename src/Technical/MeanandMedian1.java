package Technical;

import java.util.Arrays;

public class MeanandMedian1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int input1 =  5;
		int[] input2 = {1,2,2,3,3};
		System.out.println(Arrays.toString(meanMedian(input1,input2)));

	}

	private static int[] meanMedian(int input1, int[] input2) {
		// TODO Auto-generated method stub
		Arrays.sort(input2);
		int n = input2.length;
		int maxDiff = 0;
		int maxSubset[] = new int[0];
		
		for(int i=0;i<n;i++){
			for(int j=i;j<n;j++){
				int subset[]=Arrays.copyOfRange(input2, i, j+1);
				double mean = Arrays.stream(subset).average().getAsDouble();
				int median = subset[subset.length/2];
				int diff = (int) (mean-median) ;
				if(diff>maxDiff){
					maxDiff = diff;
					maxSubset = subset;
					
				}
			}
		}
		return maxSubset;
	}

}
