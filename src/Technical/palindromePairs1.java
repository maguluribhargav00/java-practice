package Technical;

public class palindromePairs1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String input1[]={"ac","bb","dd"};
		int input2 = 3;
		System.out.println(palindromePairs(input1,input2));

	}

	private static int palindromePairs(String[] input1, int input2) {
		// TODO Auto-generated method stub
		int count = 0;
		for(int i=0;i<input2;i++){
			String s1 = input1[i];
			for(int j=i+1;j<input2;j++){
				String s2 = input1[j];
				String concatenated = s1+s2;
				if(isPalindrome(concatenated)){
					count++;
				}
			}
		}
		return count;
	}

	private static boolean isPalindrome(String s) {
		// TODO Auto-generated method stub
		int charCount[] = new int[26];
		for(char c:s.toCharArray()){
			charCount[c-'a']++;
		}
		int oddCount = 0;
		for(int count:charCount){
			if(count %2 ==1){
				oddCount++;
			}
			if(oddCount>1){
				return false;
			}
		}
		return true;
	}

}
;